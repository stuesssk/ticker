#ifndef TICKER_H
#define TICKER_H

typedef struct node
{
    char symbol[6];
    size_t cents;
    char *name;
    size_t height;
    // Left Right pointers for AVL
    struct node *left;
    struct node *right;
    // Pointer to next node for priority queue
    struct node *next;
}company;

typedef struct priorityQueue
{
    size_t length;
    company *next;
} stockList;


enum
{
    MAX_COMPANY_SIZE = 64,
    MAX_TICKER_SIZE = 6,
    MAX_PRICE_MAGNITUDE = 10
};

void
stockMarketCrash(company * stock);

company*
stockCreate(char *symbol, char *name, size_t price);

company*
balanceRight(company *market);

company*
balanceLeft(company *market);

int
howTall (company *stock);

int
myBalance(company *stock);

size_t 
maximum(size_t number1, size_t number2);

company*
buildMarket(company *newNode, char *symbol, size_t cents,
            char *name);

stockList*
intializeQueue(void);

stockList *
buildPriorityQueue(stockList *portfolio, company *market);

void
marketPrint(stockList *priceQueue);

bool
searchTree(company *market, char *ticker, int priceChange, bool intialize);

#endif
