#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sysexits.h>
#include <sys/stat.h>

#include "ticker.h"

int
main(int argc, char *argv[])
{
    // You done screwed up if I don't see two, and only two, comamand line
    // arguments come through
    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <File>\n", argv[0]);
        return EX_USAGE;
    }

    struct stat st;

    //Testing for file of size 0. If any file is size 0 bytes, there
    // is guranteed to be no intersects.
    stat(argv[1], &st);
    if (st.st_size == 0)
    {
        printf("Input file is empty.\n");
        return EX_NOINPUT;
    }


    FILE *priceFile = fopen(argv[1], "r");

    // If unable to open file just end the program
    if (!priceFile)
    {
        printf("Unable to open file %s\n", argv[1]);
        return 1;
    }

    // Initialize my variables to read in from file

    // Calloc to ensure null bytes inside of all arrays needed to store
    // file input
    char *name = calloc(1, MAX_COMPANY_SIZE + 1);
    char *price = calloc(1, MAX_PRICE_MAGNITUDE + 1);
    char *ticker = calloc(1, MAX_TICKER_SIZE + 1);

    // Counter to determine how many characters since storing last value for a
    // single stock
    size_t length = 0;

    // Counter to determine how many values have been store for a stock
    size_t count = 0;

    // Storing as cents because its money (money thinks floats are the devil)
    size_t cents = 0;

    // Pointer to determine if string to number conversion worked
    char *error;

    // Initialize the AVL
    company *stockMarket = NULL;

    // buffers
    int buff = 0;
    char tempBuff[256] = { 0 };

    bool intialize = true;

    while (buff != EOF)
    {
        buff = getc(priceFile);

        switch (count)
        {
        case (0):
            if (buff == '\n' || buff == '#' || (isspace(buff) && length == 0))
            {
                length = 0;
                break;
            }
            else if (length >= MAX_TICKER_SIZE)
            {
                length = 0;
                fprintf(stderr, "%s", "Invalid stock symbol: ");
                fprintf(stderr, "%s", ticker);
                fprintf(stderr, "%s", "\n");
                memset(ticker, '\0', strlen(ticker));
            }
            if (isspace(buff))
            {
                ++count;
                length = 0;
                break;
            }
            ticker[length] = toupper(buff);
            ++length;
            break;
        case (1):
            if (buff == '.' || (isspace(buff) && length == 0))
            {
                break;
            }
            if (buff == '#' && length != 0)
            {
                // Handles case for when there is still a number to pull
                // before the # comment
                // Using a temporary buffer to eat up the comment
                fgets(tempBuff, sizeof(tempBuff), priceFile);
            }
            else if (buff == '#')
            {
                // If I can't pull a number (when # is the first thing after
                // stock ticker I can't store anything. zero everything back out
                // (only really need to zero ticker, only thing that has been
                // saved yet)and read in the rest of the line
                fgets(tempBuff, sizeof(tempBuff), priceFile);
                memset(ticker, '\0', strlen(ticker));
                break;
            }
            else if (length >= MAX_PRICE_MAGNITUDE || price[0] == '-')
            {

                length = 0;
                count = 0;
                fprintf(stderr, "%s", "Invalid stock value: ");
                fprintf(stderr, "%s", ticker);
                fprintf(stderr, "%s", "\n");
                memset(ticker, '\0', strlen(ticker));
                memset(price, '\0', strlen(price));
                fgets(tempBuff, sizeof(tempBuff), priceFile);
                break;
            }
            else if (buff == '\n' || buff == '#')
            {
                length = 0;
                count = 0;
                cents = strtoull(price, &error, 10);
                if (*error)
                {
                    fprintf(stderr, "%s", "Invalid stock value: ");
                    fprintf(stderr, "%s", ticker);
                    fprintf(stderr, "%s", "\n");
                    memset(ticker, '\0', strlen(ticker));
                    memset(price, '\0', strlen(price));
                    break;
                }
                else
                {
                    if (!searchTree(stockMarket, ticker, cents, intialize))
                    {
                        stockMarket =
                            buildMarket(stockMarket, ticker, cents, name);
                    }
                }
                memset(ticker, '\0', strlen(ticker));
                memset(name, '\0', strlen(name));
                memset(price, '\0', strlen(price));
                break;
            }

            // store value if i got a whole stock price (last char was a space),
            // or managed a partial price (stored some number before hitting
            // a #)
            if (isspace(buff))
            {
                ++count;
                length = 0;
                cents = strtoull(price, &error, 10);
                if (*error)
                {
                    fprintf(stderr, "%s", "Invalid stock value: ");
                    fprintf(stderr, "%s", ticker);
                    fprintf(stderr, "%s", "\n");
                    memset(ticker, '\0', strlen(ticker));
                    memset(price, '\0', strlen(price));
                    break;
                }
                memset(price, '\0', strlen(price));
                break;
            }

            price[length] = buff;
            ++length;
            break;
        case (2):
            // eats up any extra white space between price and Co. name
            if (isspace(buff) && length == 0)
            {
                break;
            }
            if (buff == '#')
            {
                // Using a temporary buffer to eat up the comment
                fgets(tempBuff, sizeof(tempBuff), priceFile);
            }
            else if (length == MAX_COMPANY_SIZE - 1)
            {
                // This conditional deals with the edge case of Company
                // name being greater than given largest length
                // Using a temporary buffer to eat up any remaining charaters
                fgets(tempBuff, sizeof(tempBuff), priceFile);
            }
            // store name if i got a whole stock name (last char was a \n),
            // or managed a partial name (stored some number before hitting
            // a #)
            if (buff == '\n' || buff == '#' ||
                (length == MAX_COMPANY_SIZE - 1))
            {
                length = 0;
                if (!searchTree(stockMarket, ticker, cents, intialize))
                {
                    //printf("test\n");
                    stockMarket =
                        buildMarket(stockMarket, ticker, cents, name);
                }
                memset(ticker, '\0', strlen(ticker));
                memset(name, '\0', strlen(name));
                count = 0;

                break;
            }
            name[length] = buff;
            ++length;
            break;
        }
    }


    // Intialize values need only for user input through stdin
    char buffer[256] = { 0 };
    char *token;
    double inputPrice = 0;

    intialize = false;
    printf("Please input ticker updates:\n");
    while (fgets(buffer, sizeof(buffer), stdin))
    {
        // Using strtok to store ticker symbol
        token = strtok(buffer, " \t#\n");
        if (strlen(token) < MAX_TICKER_SIZE)
        {
            strncpy(ticker, token, MAX_TICKER_SIZE);
        }
        // Using strtok to store price change
        token = strtok(NULL, " \t#\n");
        inputPrice = strtod(token, &error);

        // Handles if you have anthing not numeric in the price or
        // price exceeds the maximum inposed by the class stock market
        if (*error || inputPrice >= 1000000)
        {
            printf("Invalid price input\n");
            continue;
        }
        cents = (int) (inputPrice * 100);
        // if not in the market already, add it. If already in
        // searchTree will update the price
        if (!searchTree(stockMarket, ticker, cents, intialize))
        {
            int i = 0;

            while (ticker[i])
            {
                ticker[i] = toupper(ticker[i]);
                ++i;
            }
            stockMarket = buildMarket(stockMarket, ticker, cents, name);
        }
        memset(ticker, '\0', strlen(ticker));
    }
    // Don't need it any more, kill it, burn it with fire.
    // Valgrind overloards require us to scrafice all mallocs before
    // exiting the program
    free(name);
    free(price);
    free(ticker);
    fclose(priceFile);

    // End game, all input is done. Sort the stocks so you can display
    // them all pretty for the user
    stockList *priceQueue = intializeQueue();

    priceQueue = buildPriorityQueue(priceQueue, stockMarket);
    // Give the users what they want (printed to stdout from cheapest to most
    // most expesive stock)
    printf("\n\nResults of Stuessel Stock Market at Closing Bell:\n");
    marketPrint(priceQueue);

    // It's a free market. Let it burn to the ground if it wants
    // to destroy itself, let it.
    stockMarketCrash(stockMarket);
    // Valgrind overloards require us to scrafice all mallocs before
    // exiting the program
    free(priceQueue);
}

void
marketPrint(stockList * priceQueue)
{
    double stockPrice = 0;
    company *stock = NULL;


    while (1)
    {
        stock = priceQueue->next;
        stockPrice = ((double) stock->cents) / 100;
        printf("%s ", stock->symbol);
        printf("%.2lf ", stockPrice);
        if (stock->name != NULL)
        {
            printf("%s", stock->name);
        }
        printf("\n");
        if (stock->next == NULL)
        {
            break;
        }
        priceQueue->next = stock->next;
    }

}

void
stockMarketCrash(company * stock)
{
    // Destroy function modified from Class source code on BST function
    // Function will free any memory that was malloced in the 
    //creation of the AVL
    if (stock == NULL)
    {
        return;
    }
    stockMarketCrash(stock->left);
    stockMarketCrash(stock->right);
    // Don't need it any more, kill it all.
    // Valgrind overloards require us to scrafice all mallocs before
    // exiting the program
    free(stock->name);
    free(stock->symbol);
}

company *
stockCreate(char *symbol, char *name, size_t price)
{

    company *newStock = (company *) malloc(sizeof(company));

    if (!newStock)
    {
        printf("Memory Error");
        return NULL;
    }

    // Malloc the space for the name Valgrind will want that space
    // back so don't get to attached to it
    newStock->name = calloc(1, strlen(name) + 1);
    strncpy(newStock->name, name, strlen(name));

    // Symbol intialized as array, mallocing is not necessary
    strncpy(newStock->symbol, symbol, sizeof(newStock->symbol));

    // Storing price (always in cents)
    newStock->cents = price;
    // Initalize the pointers to NULL to be on the safe side
    newStock->left = NULL;
    newStock->right = NULL;
    newStock->next = NULL;
    //New node will initialize as leaf and height = 1
    newStock->height = 1;
    return newStock;
}


company *
balanceRight(company * marketPlace)
{
    // storing children of node to rotate
    company *temp1 = marketPlace->left;
    company *temp2 = temp1->right;

    // Rotate right subtree rooted at market node
    temp1->right = marketPlace;
    marketPlace->left = temp2;

    // Update heights
    marketPlace->height =
        maximum(howTall(marketPlace->left), howTall(marketPlace->right)) + 1;
    temp1->height = maximum(howTall(temp1->left), howTall(temp1->right)) + 1;

    return temp1;
}

company *
balanceLeft(company * marketPlace)
{
    company *temp1 = marketPlace->right;
    company *temp2 = temp1->left;

    // Rotate right subtree rooted at marketPlace node
    temp1->right = marketPlace;
    marketPlace->left = temp2;

    // Update heights
    marketPlace->height =
        maximum(howTall(marketPlace->left), howTall(marketPlace->right)) + 1;
    temp1->height = maximum(howTall(temp1->left), howTall(temp1->right)) + 1;

    return temp1;
}


int
howTall(company * stock)
{
    if (stock == NULL)
    {
        return 0;
    }
    return stock->height;
}

int
myBalance(company * stock)
{
    if (stock == NULL)
    {
        return 0;
    }
    return howTall(stock->left) - howTall(stock->right);
}

size_t
maximum(size_t number1, size_t number2)
{
    return (number1 > number2) ? number1 : number2;
}

company *
buildMarket(company * newNode, char *symbol, size_t cents, char *name)
{
    // code for AVL and balancing of tree based on code found at:
    // http://www.geeksforgeeks.org/avl-tree-set-1-insertion/

    // Insert as normall

    if (newNode == NULL)
    {
        return (stockCreate(symbol, name, cents));
    }

    if (symbol < newNode->symbol)
    {
        newNode->left = buildMarket(newNode->left, symbol, cents, name);
    }
    else if (symbol > newNode->symbol)
    {
        newNode->right = buildMarket(newNode->left, symbol, cents, name);
    }

    //Update height of this ancestor node
    newNode->height =
        1 + maximum(howTall(newNode->left), howTall(newNode->right));

    // Am I balanced
    int balance = myBalance(newNode);

    // Left Left Case
    if (balance > 1 && symbol < newNode->left->symbol)
    {
        return balanceRight(newNode);
    }

    // Right Right Case
    if (balance < -1 && symbol > newNode->right->symbol)
    {
        return balanceLeft(newNode);
    }

    // Left Right Case
    if (balance > 1 && symbol > newNode->left->symbol)
    {
        newNode->left = balanceLeft(newNode->left);
        return balanceRight(newNode);
    }

    // Right Left Case
    if (balance < -1 && symbol < newNode->right->symbol)
    {
        newNode->right = balanceRight(newNode->right);
        return balanceLeft(newNode);
    }

    //return the stock pointer
    return newNode;
}

stockList *
intializeQueue(void)
{
    stockList *marketPlace = calloc(1, sizeof(*marketPlace));

    if (!marketPlace)
    {
        printf("Memory Error");
        return NULL;
    }

    marketPlace->next = NULL;
    marketPlace->length = 0;

    return marketPlace;
}

stockList *
buildPriorityQueue(stockList * portfolio, company * marketPlace)
{
    // if market is empty, nothing to add to prioirty queue
    if (!marketPlace)
    {
        return portfolio;
    }

    if (portfolio->next)
    {
        // setting up company node to be able to traverse through
        // the linked list to find proper insertion point
        company *holdingCo = portfolio->next;

        while (1)
        {
            // most likely need to traverse some amount through the linked
            // linked list.  Make that first in if-else if-else to avoid
            // unecessay checks
            if (holdingCo->next &&
                maximum(marketPlace->cents,
                        holdingCo->next->cents) == marketPlace->cents)
            {
                // Have not found proper place in list keep going
                holdingCo = holdingCo->next;
            }
            else if (marketPlace->cents < holdingCo->cents)
            {
                // insert node at the head
                marketPlace->next = holdingCo;
                portfolio->next = marketPlace;
                // added a node, update length
                ++portfolio->length;
                break;
            }
            else
            {
                // Insert node everywhere else in linked list
                // --Adding to tail is just adding between highest value
                //   null.
                marketPlace->next = holdingCo->next;
                holdingCo->next = marketPlace;
                // added a node, update length
                ++portfolio->length;
                break;
            }
        }
    }
    else
    {
        // Adds node to the front of the queue
        portfolio->next = marketPlace;
    }

    // Cycle through to place evey company node in the linked list
    portfolio = buildPriorityQueue(portfolio, marketPlace->left);
    portfolio = buildPriorityQueue(portfolio, marketPlace->right);
    return portfolio;
}

bool
searchTree(company * marketPlace, char *ticker, int priceChange,
           bool intialize)
{
    // This function will test if the string passed exists in the BST passed to
    // the function.  The function will ignore the case of both words during the
    // comparision
    bool match = false;

    if (marketPlace)
    {
        // Need to inverse, since strcasecmp returns 0 if the
        // strings are equal.
        if (!strcasecmp(ticker, marketPlace->symbol))
        {
            if (intialize)
            {
                // Used to handle the cse when input file has duplicates
                // of a stock.
                marketPlace->cents = priceChange;
            }
            else if ((marketPlace->cents + priceChange) > 0 &&
                     (marketPlace->cents + priceChange) < 100000000000)
            {
                marketPlace->cents += priceChange;

            }
            else
            {
                // Handles cases of 
                fprintf(stderr, "%s", "Attempted INVALID stock value.\n");
            }
            match = true;
            return match;
        }

        match = searchTree(marketPlace->left, ticker, priceChange, intialize);

        if (!match)
        {
            match =
                searchTree(marketPlace->right, ticker, priceChange, intialize);
        }
    }
    return match;
}
